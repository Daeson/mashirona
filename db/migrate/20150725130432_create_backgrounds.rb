class CreateBackgrounds < ActiveRecord::Migration
  def change
    create_table :backgrounds do |t|
      t.string :image, null: false
      t.integer :page, null: false
      t.boolean :active, default: false, null: false

      t.timestamps null: false
    end
  end
end
