class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.belongs_to :product, index: true, foreign_key: true
      t.string :image, null: false
      t.boolean :cover, default: false, null: false
      t.boolean :three_sixty, default: false, null: false

      t.timestamps null: false
    end
  end
end
