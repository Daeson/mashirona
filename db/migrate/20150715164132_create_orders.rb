class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :cart, index: true, foreign_key: true, null: false
      t.string :phone, null: false
      t.string :email, null: false
      t.string :ip_address, null: false
      t.string :transaction_id, null: false

      t.timestamps null: false
    end
  end
end
