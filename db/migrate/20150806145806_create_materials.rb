class CreateMaterials < ActiveRecord::Migration
  def change
    create_table :materials do |t|
      t.string :name, null: false
      t.string :image, null: false
      t.text :description, null: false
      t.belongs_to :product, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
