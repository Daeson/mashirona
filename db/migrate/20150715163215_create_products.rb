class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.integer :price_cents, null: false
      t.integer :length, null: false
      t.integer :width, null: false
      t.integer :height, null: false
      t.integer :limited_to, default: 0, null: false
      t.integer :stock, default: 0, null: false

      t.timestamps null: false
    end
  end
end
