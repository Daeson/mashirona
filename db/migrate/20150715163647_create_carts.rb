class CreateCarts < ActiveRecord::Migration
  def change
    create_table :carts do |t|
      t.belongs_to :visitor, index: true, foreign_key: true, null: false
      t.boolean :purchased, default: false, null: false

      t.timestamps null: false
    end
  end
end
