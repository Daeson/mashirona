class CreateShippings < ActiveRecord::Migration
  def change
    create_table :shippings do |t|
      t.belongs_to :order, index: true, foreign_key: true
      t.string :name, null: false
      t.string :street_address, null: false
      t.string :extended_address
      t.string :city
      t.string :state
      t.string :postal_code
      t.string :country, null: false

      t.timestamps null: false
    end
  end
end
