class CreateVisitors < ActiveRecord::Migration
  def change
    create_table :visitors do |t|
      t.integer :visit_count, default: 1, null: false
      t.datetime :current_visit_at, null: false
      t.datetime :last_visit_at, null: false

      t.timestamps null: false
    end
  end
end
