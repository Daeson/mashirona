# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160303160633) do

  create_table "admins", force: :cascade do |t|
    t.string   "email",              default: "", null: false
    t.string   "encrypted_password", default: "", null: false
    t.integer  "sign_in_count",      default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",    default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true

  create_table "backgrounds", force: :cascade do |t|
    t.string   "image",                      null: false
    t.integer  "page",                       null: false
    t.boolean  "active",     default: false, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "billings", force: :cascade do |t|
    t.integer  "order_id"
    t.string   "name",             null: false
    t.string   "street_address",   null: false
    t.string   "extended_address"
    t.string   "city"
    t.string   "state"
    t.string   "postal_code"
    t.string   "country",          null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "billings", ["order_id"], name: "index_billings_on_order_id"

  create_table "cart_items", force: :cascade do |t|
    t.integer  "product_id",             null: false
    t.integer  "cart_id",                null: false
    t.integer  "quantity",   default: 1, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "cart_items", ["cart_id"], name: "index_cart_items_on_cart_id"
  add_index "cart_items", ["product_id"], name: "index_cart_items_on_product_id"

  create_table "carts", force: :cascade do |t|
    t.integer  "visitor_id",                 null: false
    t.boolean  "purchased",  default: false, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "carts", ["visitor_id"], name: "index_carts_on_visitor_id"

  create_table "contacts", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "materials", force: :cascade do |t|
    t.string   "name",        null: false
    t.string   "image",       null: false
    t.text     "description", null: false
    t.integer  "product_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "materials", ["product_id"], name: "index_materials_on_product_id"

  create_table "orders", force: :cascade do |t|
    t.integer  "cart_id",        null: false
    t.string   "phone",          null: false
    t.string   "email",          null: false
    t.string   "ip_address",     null: false
    t.string   "transaction_id", null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "orders", ["cart_id"], name: "index_orders_on_cart_id"

  create_table "photos", force: :cascade do |t|
    t.integer  "product_id"
    t.string   "image",                       null: false
    t.boolean  "cover",       default: false, null: false
    t.boolean  "three_sixty", default: false, null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "photos", ["product_id"], name: "index_photos_on_product_id"

  create_table "products", force: :cascade do |t|
    t.integer  "price_cents",             null: false
    t.integer  "length",                  null: false
    t.integer  "width",                   null: false
    t.integer  "height",                  null: false
    t.integer  "limited_to",  default: 0, null: false
    t.integer  "stock",       default: 0, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "shippings", force: :cascade do |t|
    t.integer  "order_id"
    t.string   "name",             null: false
    t.string   "street_address",   null: false
    t.string   "extended_address"
    t.string   "city"
    t.string   "state"
    t.string   "postal_code"
    t.string   "country",          null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "shippings", ["order_id"], name: "index_shippings_on_order_id"

  create_table "visitors", force: :cascade do |t|
    t.integer  "visit_count",      default: 1, null: false
    t.datetime "current_visit_at",             null: false
    t.datetime "last_visit_at",                null: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

end
