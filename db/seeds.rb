#Cloudinary::Api.delete_all_resources

home_photo = []
home_photo << 'http://placeholdit.imgix.net/~text?txtsize=150&txt=home%201&w=2000&h=1500'
home_photo << 'http://placeholdit.imgix.net/~text?txtsize=150&txt=home%202&w=2000&h=1500'
home_photo << 'http://placeholdit.imgix.net/~text?txtsize=150&txt=home%203&w=2000&h=1500'
while !home_photo.blank?
    background = Background.create!(remote_image_url: home_photo.shift, page: 'home', active: true)
end

about_photo = []
about_photo << 'http://placeholdit.imgix.net/~text?txtsize=150&txt=about%201&w=2000&h=1000'
about_photo << 'http://placeholdit.imgix.net/~text?txtsize=150&txt=about%202&w=2000&h=1500'
about_photo << 'http://placeholdit.imgix.net/~text?txtsize=150&txt=about%203&w=2000&h=2000'
about_photo << 'http://placeholdit.imgix.net/~text?txtsize=150&txt=about%204&w=2000&h=1500'
about_photo << 'http://placeholdit.imgix.net/~text?txtsize=150&txt=about%205&w=2000&h=1000'
while !about_photo.blank?
    background = Background.create!(remote_image_url: about_photo.shift, page: 'about', active: true)
end

photos_url = []
photos_url << 'http://placeholdit.imgix.net/~text?txtsize=47&txt=500%C3%97500&w=500&h=500'
photos_url << 'http://placeholdit.imgix.net/~text?txtsize=56&txt=600%C3%97500&w=600&h=500'
photos_url << 'http://placeholdit.imgix.net/~text?txtsize=47&txt=500%C3%97600&w=500&h=600'
extra_photos_url = []
extra_photos_url << 'http://placeholdit.imgix.net/~text?txtsize=75&txt=1500%C3%97300&w=1500&h=300'
extra_photos_url << 'http://placeholdit.imgix.net/~text?txtsize=75&txt=1300%C3%97300&w=1300&h=300'
extra_photos_url << 'http://placeholdit.imgix.net/~text?txtsize=75&txt=2000%C3%97300&w=2000&h=300'
25.times do |i|
    price = Faker::Commerce.price
    dmsn = i + 1
    if i.odd?
        limited_to = Random.rand(100..200)
        stock = Random.rand(0..limited_to)
    else
        limited_to = 0
        stock = Random.rand(0..100)
    end
    product = Product.new(price: price, length: dmsn, width: dmsn, height: dmsn, limited_to: limited_to, stock: stock)

    j = 0
    photo = [product.photos.build(remote_image_url: photos_url.sample, three_sixty: true, cover: true)]
    while j < Random.rand(3...10)
        photo << product.photos.build(remote_image_url: extra_photos_url.sample, three_sixty: false, cover: false)
        j += 1
    end
    product.photos = photo
    product.save!
end

Admin.create(email: "tehsoonts@gmail.com", password: "password", current_sign_in_at: Time.now, current_sign_in_ip: "::1")
9.times do
    email = Faker::Internet.email
    password = "password"
    current_sign_in_at = Time.now
    current_sign_in_ip = Faker::Internet.ip_v4_address

    Admin.create(email: email, password: password, current_sign_in_at: current_sign_in_at, current_sign_in_ip: current_sign_in_ip)
end
