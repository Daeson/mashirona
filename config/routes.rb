Rails.application.routes.draw do
    #standard route
    root      'pages#home'
    match     'contact' => 'contacts#new', via: [:get, :post]
    resources :products
    resources :cart_items, only: [:create, :update, :destroy], constraints: {format: 'js'}
    match     'cart' => 'carts#show', via: :get
    resources :carts, only: [:index, :destroy]
    match     'checkout' => 'orders#new', via: :get
    resources :orders, except: [:new]

    #devise
    devise_for :admins, skip: [:registrations, :unlock]
    devise_scope :admin do
        post "/admins/unlock" => "devise/unlocks#create", as: :admin_unlock
        get "/admins/unlock" => "devise/unlocks#show", as: ''
    end

    #console
    get 'console', to: 'console#core'
    #admin manager
    resources :admins, except: [:index, :show]
end
