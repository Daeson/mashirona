class CartItem < ActiveRecord::Base
  belongs_to :product
  belongs_to :cart

  validates :product,
            presence: true
  validates :cart,
            presence: true
  validates :quantity,
            numericality: {greater_than: 0, less_than_or_equal_to: :stock_quantity, only_integer: true}

  def sum
    product.price * quantity
  end

  def purchase
    product.purchase(quantity)
  end

  private
  def stock_quantity
    product.stock
  end
end
