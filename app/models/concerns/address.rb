module Address
  extend ActiveSupport::Concern
  included do
    validates :order,
      presence: true
    validates :name,
      presence: true,
      format: { without: /[0-9]/, message: 'should not contain numbers' },
      length: { maximum: 50 }
    validates :street_address,
      presence: true,
      length: { maximum: 200 }
    validates :extended_address,
      length: { maximum: 200 }
    validates :city,
      presence: true,
      length: { maximum: 100 }
    validates :state,
      length: { maximum: 50 }
    validates :postal_code,
      length: { maximum: 20 }
    validates :country,
      presence: { message: 'select a country' }

    before_save :clean_up
  end

  private
  def clean_up
    self.attributes.each do |attr_name, attr_value|
      unless attr_name == 'order_id'
        squeeze!(attr_name, attr_value)
      end
    end
  end

  def squeeze!(attribute, value)
    write_attribute(attribute, value.to_s.strip.upcase.squish)
  end
end
