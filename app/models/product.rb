class Product < ActiveRecord::Base
  has_many :photos, inverse_of: :product, dependent: :destroy
  has_many :materials, dependent: :destroy
  has_many :cart_items, dependent: :destroy

  accepts_nested_attributes_for :photos, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :materials, allow_destroy: true, reject_if: :all_blank

  default_scope -> { order(created_at: :desc) }

  monetize :price_cents

  validates :price,
    presence: true,
    numericality: { greater_than: 0 }
  validates :photos,
    presence: true
  validates :length, :width, :height,
    presence: true,
    numericality: { greater_than_or_equal_to: 1, only_integer: true }
  validates :limited_to,
    numericality: { greater_than_or_equal_to: 0, only_integer: true }
  validates :stock,
    presence: true,
    numericality: { greater_than_or_equal_to: 0, only_integer: true }

  def limited?
    limited_to > 0
  end

  def humanized_dimensions(l='L', w='W', h='H')
    return "#{l}: #{length}cm <strong>x</strong> #{w}: #{width}cm <strong>x</strong> #{h}: #{height}cm".html_safe
  end

  def purchase(quantity)
    self.stock -= quantity
  end
end
