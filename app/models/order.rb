class Order < ActiveRecord::Base
  belongs_to :cart
  has_one :shipping, inverse_of: :order, dependent: :destroy
  has_one :billing, inverse_of: :order, dependent: :destroy

  accepts_nested_attributes_for :shipping, reject_if: :all_blank
  accepts_nested_attributes_for :billing, reject_if: :all_blank

  validate :suffice_stock?
  validates :cart,
    presence: true
  validates :phone,
    presence: true,
    length: { maximum: 30 }
  validates :email,
    presence: true,
    length: { maximum: 50 },
    format: { with: /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i }
  validates :ip_address,
    presence: true,
    format: { with: Resolv::AddressRegex }
  validates :transaction_id,
    length: { maximum: 10 }

  before_save :clean_up
  after_save :mark_as_purchased

  def purchase_with(payment_nonce)
    @result = Braintree::Transaction.sale(
      amount: cart.total,
      payment_method_nonce: payment_nonce,
      customer: {
        first_name: billing.name,
        phone: phone,
        email: email
      },
      billing: {
        first_name: billing.name,
        street_address: billing.street_address,
        extended_address: billing.extended_address,
        locality: billing.city,
        region: billing.state,
        postal_code: billing.postal_code,
        country_name: billing.country
      },
      shipping: {
        first_name: shipping.name,
        street_address: shipping.street_address,
        extended_address: shipping.extended_address,
        locality: shipping.city,
        region: shipping.state,
        postal_code: shipping.postal_code,
        country_name: shipping.country
      },
      options: {
        submit_for_settlement: true
      }
    )

    puts_result
  end

  private
  def suffice_stock?

  end

  def clean_up
    self.email = email.strip.downcase
  end

  def puts_result
    if @result.success?
      puts "success!: #{@result.transaction.id}"
      self.transaction_id = @result.transaction.id
      return true
    elsif @result.transaction
      puts "Error processing transaction:"
      puts "  code: #{@result.transaction.processor_@result_code}"
      puts "  text: #{@result.transaction.processor_@result_text}"
    else
      @result.errors.each do |error|
        puts error.message
        return false
      end
    end
  end

  def mark_as_purchased
    cart.purchase
  end
end
