class Visitor < ActiveRecord::Base
  has_many :carts

  validates :visit_count,
    numericality: { greater_than_or_equal_to: 1, only_integer: true }

  before_create :init

  def visiting
    if last_visit_at < 2.hours.ago
      self.current_visit_at = self.last_visit_at = Time.now
      self.visit_count += 1
    else
      current_visit_at = Time.now
    end
  end

  private
  def init
    self.current_visit_at = self.last_visit_at = Time.now
  end
end
