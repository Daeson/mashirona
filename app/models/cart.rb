class Cart < ActiveRecord::Base
  belongs_to :visitor
  has_many :cart_items, :dependent => :destroy
  has_one :order

  validates :visitor,
    presence: true

  def add_product(product_id)
    if Product.find(product_id) && !( cart_item = included?(product_id))
      return cart_items.create(product_id: product_id)
    else
      return cart_item
    end
  rescue ActiveRecord::RecordNotFound
    nil
  end

  def included?(product_id)
    cart_items.find_by(product_id: product_id)
  end

  def total
    cart_items.to_a.sum { |item| item.sum }
  end

  def purchase
    self.purchased = true
    cart_items.to_a.each { |item| item.purchase }
  end
end
