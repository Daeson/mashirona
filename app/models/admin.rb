class Admin < ActiveRecord::Base
    # Include default devise modules. Others available are:
    # :confirmable, :recoverable, :rememberable, :timeoutable and :omniauthable
    devise :database_authenticatable, :registerable,
        :trackable, :validatable, :lockable
end
