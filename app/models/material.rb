class Material < ActiveRecord::Base
  belongs_to :product
  mount_uploader :image, ImageUploader

  validates :image,
    presence: true
  validates :name,
    presence: true
  validates :description,
    presence: true
end
