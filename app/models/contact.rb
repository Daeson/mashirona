class Contact < MailForm::Base
  attributes :name, validate: true
  attributes :email, validate: /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attributes :subject,  validate: ["Enquiry", "Feedback", "Support"]
  attributes :message, validate: true
  attributes :nickname,  captcha: true

  append :remote_ip, :user_agent

  def headers
    {
      subject: "Site Contact - #{subject}",
      to: "gristlegrime.dev@gmail.com",
      from: %("#{name}" <#{email}>)
    }
  end
end
