class Background < ActiveRecord::Base
  mount_uploader :image, ImageUploader

  validates :image,
    presence: true
  validates :page,
    presence: true

  enum page: [:home, :about]
end
