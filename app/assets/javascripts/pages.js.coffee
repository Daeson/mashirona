hover = false
status = 0
scrolled = false
ticker = null

$(document).on 'ready page:load', ->
  if window.location.pathname == '/'
    sidenav_init()
    scroller_init()
  else
    window.clearInterval(ticker)

sidenav_init = ->
  if $(window).scrollTop() < $(window).height() / 5
    $(".side-nav").css("margin-left", "100%")

  $(window).on 'scroll', ->
    scrolled = true


  ticker = setInterval (->
    if scrolled
      scrolled = false
      toggle_sidenav() if not hover
  ), 500


  $(".side-navigation").mouseenter ->
    hover = true
    sidenav_status(1)

  $(".side-navigation").mouseleave ->
    hover = false
    toggle_sidenav()

toggle_sidenav = ->
  if $(window).scrollTop() < $(window).height() / 8
    sidenav_status(0)
    $("li > a[href='/#about']").parent().removeClass("active")
    $("li > a[href='/']").parent().addClass("active")
  else
    sidenav_status(1)
    $("li > a[href='/']").parent().removeClass("active")
    $("li > a[href='/#about']").parent().addClass("active")

sidenav_status = (stat) ->
  if stat != status
    if stat
      status = 1
      $(".side-nav").stop().animate
        "margin-left": "0%"
      ,
        duration: 500
        queue: false
        easing: "easeOutCubic"
    else
      status = 0
      $(".side-nav").stop().animate
        "margin-left": "100%"
      ,
        duration: 500
        queue: false
        easing: "easeInCubic"

scroller_init = ->
  $("li > a").click (e) ->
    if $(this).attr("href") == '/#about'
      e.preventDefault()
      $("html, body").animate
        scrollTop: $(window).height()
      ,
        duration: 400
        queue: false
        easing: "easeOutCubic"
      $("li > a[href='/']").parent().removeClass("active")
      $("li > a[href='/#about']").parent().addClass("active")
    else if $(this).attr("href") == '/'
      e.preventDefault()
      $("html, body").animate
        scrollTop: 0
      ,
        duration: 400
        queue: false
        easing: "easeOutCubic"
      $("li > a[href='/#about']").parent().removeClass("active")
      $("li > a[href='/']").parent().addClass("active")