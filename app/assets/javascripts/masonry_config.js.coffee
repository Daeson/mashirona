$(document).on 'ready page:load', ->
	if $(".grid").length
		$(".grid").masonry
			itemSelector: ".grid-item"
	
		$(".grid").imagesLoaded().progress ->
	    	$(".grid").masonry()