loading = false
$progress_bar = null
$steps = null
$sections = null
previous_index = null
current_index = null

$(document).on 'ready page:load', ->
  if $("input").length and $(".progress").length
    init_variable()
    init_progress()
    init_validation()
  else
    $(".progress_arrow").hide()

init_variable = ->
  $progress_bar = $(".progress > .determinate")
  $steps = $("a.step")
  $sections = $("div.form-section")
  previous_index = 0
  current_index = 0

init_progress = ->
  $sections.hide()
  $steps.eq(current_index).parent().addClass("current_step")
  $sections.eq(current_index).addClass("current_section").show()
  load_progress_bar()
  init_click()
  $("#prev_progress").hide()

init_validation = ->
  $("input, select").on "invalid", (e) ->
    generate_error_message($(this))

init_click = ->
  $steps.click ->
    check_progress($steps.index($(this)))

  $("#prev_progress").click ->
    check_progress(current_index - 1)

  $("#next_progress").click ->
    check_progress(current_index + 1)

check_progress = (new_index) ->
  if $(".animated").length == 0 and not loading
    $steps.off()
    $("#prev_progress").off()
    $("#next_progress").off()
    loading = true

    setTimeout (->
      loading = false
    ), 1000

    current_index = new_index
    unless current_index is previous_index
      if $(".indecisive").length
        current_index = previous_index
        alert "Please confirm the change(s)"
      else if current_index < previous_index or validate_section()
        silent_validate()
        $(".current_section").removeClass("current_section")
        $sections.eq(current_index).addClass("current_section")
        load_progress()
        previous_index = current_index
      else
        current_index = previous_index
        alert "Please correct the following"

      update_arrows()

    init_click()

update_arrows = ->
  if current_index is 0
    $("#prev_progress").hide()
    $("#next_progress").show()
  else if current_index is 2
    $("#prev_progress").show()
    $("#next_progress").hide()
  else
    $("#prev_progress").show()
    $("#next_progress").show()

load_progress = ->
  prev_section = $sections.eq(previous_index)
  curr_section = $sections.eq(current_index)
  if current_index < previous_index
    animate(prev_section, "fadeOutRight", curr_section, "fadeInLeft")
  else if current_index > previous_index
    animate(prev_section, "fadeOutLeft", curr_section, "fadeInRight")
  load_progress_bar()

load_progress_bar = ->
  unless current_index is previous_index
    $(".current_step").removeClass("current_step")
    $progress_bar.width($steps.eq(current_index).attr("progress"))
    $progress_bar.one "webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", ->
      $steps.eq(current_index).parent().addClass("current_step")

animate = ($element, type, $next_element = null, next_type = null) ->
  $element.addClass("animated " + type)
  $element.one "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", ->
    $element.removeClass("animated " + type)
    if $next_element
      $element.hide()
      $next_element.show()
      $next_element.addClass("animated " + next_type)
      $next_element.one "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", ->
        $next_element.removeClass("animated " + next_type)
        false
      false
    false
  false

validate_section = ->
  valid_section = true
  $(".error").remove()
  if $(".current_section input, .current_section select").length
    $(".current_section input, .current_section select").each ->
      unless $(this)[0].checkValidity()
        valid_section = false
        true

  return valid_section

silent_validate = ->
  i = current_index - previous_index
  return if i is 1

  valid_section = true
  counter = 0
  while counter < i - 1
    counter += 1
    $val_elem = $sections.eq(counter).find("input, select")
    if $val_elem.length
      $val_elem.each ->
        unless $(this)[0].validity.valid
          valid_section = false


  if not valid_section
    current_index = counter

generate_error_message = ($element) ->
  field_wrapper = $element.parents(".input-field")
  field_wrapper.find(".error").remove()
  field_wrapper.prepend("<span class='error'>" + get_fail_message($element) + "</span>")
  animate(field_wrapper, "shake")

get_fail_message = ($element) ->
  valid_state = $element[0].validity
  return $element[0].validationMessage