$(document).on('ready page:load', function () {
    if ($('#threesixty').length === 0) {
        return;
    }

    var preLoad = imagesLoaded('#spinner-bg'),
        imgLoad = null,
        ready = false,
        dragging = false,
        pointerStartPosX = 0,
        pointerEndPosX = 0,
        pointerDistance = 0,

        monitorStartTime = 0,
        monitorInt = 10,
        ticker = 0,
        speedMultiplier = 8,

        totalFrames = 0,
        currentFrame = 0,
        frames = [],
        endFrame = 0,
        loadedImages = 0,

        $document = $(document),
        $container = $('#threesixty'),
        $images = $('#threesixty_images'),

        demoMode = false,
        fakePointer = {
            x: 0,
            speed: 0.1
        },
        fakePointerTimer = 0;

    function preload() {
        var threesixty_src = [];
        $("#threesixty_images img").each(function () {
            threesixty_src.push($(this).attr("src"));
            $(this).removeAttr("src");
        });

        preLoad.on('done', function () {
            $("#threesixty_images img").each(function () {
                $(this).attr("src", threesixty_src.pop());
            });

            imgLoad = imagesLoaded('#threesixty_images');
            totalFrames = imgLoad.images.length;
            loadImage();
        });
    }

    function loadImage() {
        imgLoad.on('done', function () {
            for (var i = 0, len = imgLoad.images.length; i < len; i++) {
                var $image = $(imgLoad.images[i].img);
                $image.addClass("previous-image");
                frames.push($image);
                imageLoaded();
            }
        });

        imgLoad.on('progress', function () {
            loadedImages++;
            $("#spinner span").text(Math.floor(loadedImages / totalFrames * 100) + "%");
        });
    }

    function imageLoaded() {
        if (loadedImages == totalFrames) {
            $("#spinner").hide();
            frames[0].removeClass("previous-image").addClass("current-image");
            showThreesixty();
        }
    }

    function showThreesixty() {
        $images.fadeIn("slow");
        ready = true;
        endFrame = -totalFrames;

        if (!demoMode) {
            refresh();
        } else {
            fakePointerTimer = window.setInterval(moveFakePointer, 100);
        }
    }

    function moveFakePointer() {
        if (currentFrame == totalFrames) {
            quitDemoMode();
            return;
        }

        fakePointer.x += fakePointer.speed;
        trackPointer();
    }

    function quitDemoMode() {
        window.clearInterval(fakePointerTimer);
        demoMode = false;
    }

    preload();

    function render() {
        if (currentFrame !== endFrame) {
            var frameEasing = endFrame < currentFrame ? Math.floor((endFrame - currentFrame) * 0.1) : Math.ceil((endFrame - currentFrame) * 0.1);
            hidePreviousFrame();
            currentFrame += frameEasing;
            showCurrentFrame();
        } else {
            window.clearInterval(ticker);
            ticker = 0;
        }
    }

    function refresh() {
        if (ticker === 0) {
            ticker = self.setInterval(render, Math.round(3000 / 60));
        }
    }

    function hidePreviousFrame() {
        frames[getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image");
    }

    function showCurrentFrame() {
        frames[getNormalizedCurrentFrame()].removeClass("previous-image").addClass("current-image");
    }

    function getNormalizedCurrentFrame() {
        var c = -Math.ceil(currentFrame % totalFrames);
        if (c < 0) c += (totalFrames - 1);
        return c;
    }

    function getPointerEvent(event) {
        return event.originalEvent.targetTouches ? event.originalEvent.targetTouches[0] : event;
    }

    $container.on("mousedown", function (event) {
        quitDemoMode();

        event.preventDefault();
        pointerStartPosX = getPointerEvent(event).pageX;
        dragging = true;
    });

    $document.on("mouseup", function (event) {
        event.preventDefault();
        dragging = false;
    });

    $document.on("mousemove", function (event) {
        if (demoMode) {
            return;
        }

        event.preventDefault();
        trackPointer(event);
    });
    $container.on("touchstart", function (event) {
        quitDemoMode();

        //event.preventDefault();
        pointerStartPosX = getPointerEvent(event).pageX;
        dragging = true;
    });

    $container.on("touchmove", function (event) {
        //event.preventDefault();
        trackPointer(event);
    });

    $container.on("touchend", function (event) {
        //event.preventDefault();
        dragging = false;
    });

    function trackPointer(event) {
        var userDragging = ready && dragging ? true : false;
        var demoDragging = demoMode;

        if (userDragging || demoDragging) {
            pointerEndPosX = userDragging ? getPointerEvent(event).pageX : fakePointer.x;

            if (monitorStartTime < new Date().getTime() - monitorInt) {
                pointerDistance = pointerEndPosX - pointerStartPosX;
                endFrame = currentFrame + Math.ceil((totalFrames - 1) * speedMultiplier * (pointerDistance / $container.width()));
                refresh();
                monitorStartTime = new Date().getTime();

                pointerStartPosX = userDragging ? getPointerEvent(event).pageX : fakePointer.x;
            }
        } else {
            return;
        }
    }
});