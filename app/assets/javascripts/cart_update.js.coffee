$(document).on "ready page:load", ->
  if $("form.edit_cart_item")
    init()
    $("span.description").on "reinit", ->
      $("form.edit_cart_item input").off()
      init()


init = ->
  $("form.edit_cart_item > .update-icon").hide()
  $("form.edit_cart_item input").on "input", ->
    $dirty_input = $(this)
    $dirty_input.addClass("indecisive")
    buttons = $dirty_input.parents("form.edit_cart_item").find(".update-icon").show()
    $dirty_input.parent().siblings(".update-icon").one "click", (e) ->
      if $(this).attr("id") is "cancel" or not $dirty_input.val().length or $dirty_input.val() < 1
        e.preventDefault()
        $dirty_input.val($dirty_input.attr("placeholder"))
        buttons.hide()
        $dirty_input.removeClass("indecisive")
      else if $dirty_input[0].validity.valid
        $dirty_input.siblings(".error").remove()
        $dirty_input.siblings("#cancel").hide()
        $dirty_input.removeClass("indecisive")