#/////////////////
#/configuration///
#/////////////////
buffer_at = 200
error_reload_in = 1000
error_retry_count = 5
#//////////////////

loading = false

$(document).on 'ready page:load', ->
	if $('#infinite-pagination').length and $('.pagination').length
		$(window).on 'scroll', ->
			if !loading && $(window).scrollTop() > $(document).height() - $(window).height() - buffer_at
				loading = true
				load()
	else
		$(window).off('scroll')

load = ->
	next_path = $('#infinite-pagination .pagination a[rel=next]').attr('href')
	$('#infinite-pagination .pagination').html('<span>loading...</span>')
	$.getScript next_path
		.done =>
			loading = false
		.fail =>
			if error_retry_count--
				setTimeout(load, error_reload_in)
			else
				$('#infinite-pagination .pagination').html('<a id="reload_pagination">Fail to load, click to retry</a>')
				$('#reload_pagination').click ->
					load()