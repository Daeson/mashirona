$(document).on 'ready page:load', ->
  navigation_init()
  waves_init()
  input_init()
  select_init()
  tooltip_init()
  keybinding_init()
  tabs_init()

navigation_init = ->
  $("li > a[href='" + window.location.pathname + window.location.hash + "']").parent().addClass('active')

waves_init = ->
  if $("waves-effect").length
    Waves.displayEffect()

input_init = ->
  if $("input").length
    Materialize.updateTextFields()

select_init = ->
  if $("select").length
    $("select").removeClass("browser-default")
    $("select + label").removeClass("active")
    $("select").prop("required", true)
    $("select option:selected").each ->
      if $(this).val().length == 0
        $(this).prop("disabled", true)
    $("select").material_select()

tooltip_init = ->
  $('.tooltipped').tooltip()

keybinding_init = ->
  if $("input").length
    $(document).keypress (evt) ->
      evt = (evt) ? evt: ((event) ? event: null)
      evtT = evt.target
      evtS = evt.srcElement
      node = (evtT) ? evtT: ((evtS) ? evtS: null)
      if evt.keyCode == 13 and node.type == 'text'
        return false
      return

tabs_init = ->
    $('ul.tabs').tabs()
