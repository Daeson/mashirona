$(document).on 'ready page:load', ->
	if $("a#address_duplicate").length
		order_country_input = ".order_billing_country > div.select-wrapper > input.select-dropdown"
		order_country_select = ".order_billing_country > div.select-wrapper > select.country"
		shipping_country_input = order_country_input.replace("billing", "shipping")
		shipping_country_select = order_country_select.replace("billing", "shipping")
		previous_data = {}

		$("a#address_duplicate").click ->
			$(".shipping").each ->
				id = "#" + $(this).attr("id").replace("shipping", "billing")
				previous_data[id] = $(id).val()
				unless $(this).val() is previous_data[id]
					$(id).val($(this).val()).trigger("input")
					if $(id).val().length
						$(id).next().addClass("nope")
					else
						$(id).next().removeClass("nope")
			previous_data[order_country_input] = $(order_country_input).val()
			$(order_country_input).val($(shipping_country_input).val()).trigger("close")
			previous_data[order_country_select] = $(order_country_select).val()
			$(order_country_select).val($(shipping_country_select).val())
			Materialize.updateTextFields()

			notify(previous_data)

notify = (previous_data) ->
	toast_message = "<span>Copied</span><a id='undo_duplicate' class='btn-flat yellow-text''>Undo<a>"
	Materialize.toast(toast_message, 3000)
	$("a#undo_duplicate").one "click", ->
		$.each previous_data, (key, value) ->
			$(key).val(value)
		Materialize.updateTextFields()
