$(document).on 'ready page:load', ->
	if $(".upload").length
		$(".upload").change ->
			unless typeof (FileReader) is "undefined"
				preview = get_preview_element($(this))
				regex = /\.(jpg|jpeg|png)$/i
				$($(this)[0].files).each ->
					file = $(this)
					if regex.test(file[0].name.toLowerCase())
						reader = new FileReader()
						reader.onload = (e) ->
							img = $("<img />")
							img.attr "src", e.target.result
							preview.append img
	
						reader.readAsDataURL file[0]
					else
						alert file[0].name + " is not a valid image file."
						preview.html ""
						false
			else
				alert "This browser does not support HTML5 FileReader."
	
		$(".info").on "input", ->
			preview = get_preview_element($(this))
			preview.append($(this).val())

get_preview_element = (elem) ->
	preview_type = elem.attr('id').replace('_upload', '').replace('_info', '')
	preview_id = '#' + preview_type + '_preview'
	preview = $(preview_id)
	preview.html ""

	#type specific
	if elem.val().length or elem.val()
		switch preview_type
			when 'price'
				preview.html('$')
			when 'length'
				preview.html('L: ')
			when 'width'
				preview.html('&emsp;W: ')
			when 'height'
				preview.html('&emsp;H: ')

	return preview