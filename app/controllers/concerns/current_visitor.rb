module CurrentVisitor
  extend ActiveSupport::Concern

  included do
    before_action :get_visitor
  end

  private
  def get_visitor
    unless id = session[:id]
      id = session[:id] = cookies.signed[:id]
    end
    @visitor = Visitor.find(id)
    @visitor.visiting
  rescue ActiveRecord::RecordNotFound
    get_new_visitor
  end

  def get_new_visitor
    @visitor = Visitor.create
    session[:id] = cookies.permanent.signed[:id] = @visitor.id
  end
end
