module CurrentCart
  extend ActiveSupport::Concern

  private
  def get_cart!
    unless get_cart
      get_new_cart
    end
  end

  def get_cart
    @cart = @visitor.carts.last
  end

  def get_new_cart
    @cart = @visitor.carts.create
  end

  def cart?
    !@cart.nil? && !@cart.cart_items.blank?
  end
end
