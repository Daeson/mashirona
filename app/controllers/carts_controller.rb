class CartsController < ApplicationController
  include CurrentCart
  before_action :get_cart

  def index
    @carts = Cart.all
  end

  def show
  end

  def destroy
    @cart.destroy
    redirect_to carts_url, notice: 'Cart was successfully destroyed.'
  end
end
