class OrdersController < ApplicationController
    include CurrentCart

    before_action :set_format
    before_action :get_cart
    before_action :set_order, only: [:show, :edit, :update, :destroy]

    def index
        @orders = Order.all
    end

    def show
    end

    def new
        if cart?
            @order = Order.new
            build_nested
            set_client_token
        end
    end

    def edit
    end

    def create
        @order = @cart.build_order(order_params)
        @order.ip_address = request.remote_ip
        if @order.valid? && @order.purchase_with(params[:payment_method_nonce])
            @order.save!
            get_new_cart
            redirect_to products_url, notice: 'Order was successfully created.'
        else
            build_nested
            @order.valid?
            set_client_token
            render :new
        end
    end

    def update
        if @order.update(order_params)
            redirect_to @order, notice: 'Order was successfully updated.'
        else
            render :edit
        end
    end
    def destroy
        if @order.destroy
            redirect_to orders_url, notice: 'Order was successfully destroyed.'
        else
            redirect_to orders_url, alert: 'Something went wrong.'
        end
    end

    private
    def set_format
        request.format = 'html'
    end

    def set_order
        @order = Order.find(params[:id])
    end

    def order_params
        params.require(:order).permit(:phone, :email,
                                    shipping_attributes: [:name, :street_address, :extended_address, :city, :state, :postal_code, :country],
                                    billing_attributes: [:name, :street_address, :extended_address, :city, :state, :postal_code, :country])
    end

    def build_nested
        @order.build_shipping unless @order.shipping
        @order.build_billing unless @order.billing
    end

    def set_client_token
        gon.client_token = Braintree::ClientToken.generate
    end
end
