class PagesController < ApplicationController
  def home
    @home = Background.home.where(active: true).sample
    @abouts = Background.about.where(active: true)
  end
end
