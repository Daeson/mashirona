class ProductsController < ApplicationController
    include CurrentCart
    before_action :masked_authenticate!, except: [:index, :show]
    before_action :get_cart
    before_action :set_product, only: [:show, :edit, :update, :destroy]

    def index
        @products = Product.where("stock > ?", 0).page(params[:page]).per(5)
        respond_to do |format|
            format.html {  }
            format.js {  }
        end
    end

    def show
        @three_sixties = @product.photos.where(three_sixty: true)
        @extras = @product.photos.where(three_sixty: false)
        @materials = @product.materials
    end

    def new
        @product = Product.new
    end

    def edit
    end

    def create
        @product = Product.new(product_params)
        manage_photo
        if @product.save
            redirect_to @product, notice: 'Product was successfully created.'
        else
            render :new
        end
    end

    def update
        manage_photo
        if @product.update(product_params)
            redirect_to @product, notice: 'Product was successfully updated.'
        else
            render :edit
        end
    end

    def destroy
        @product.destroy
        redirect_to products_url, notice: 'Product was successfully destroyed.'
    end

    private

    def set_product
        @product = Product.find(params[:id])
    end

    def product_params
        params.require(:product).permit(:price, :length, :width, :height, :limited_to, :stock, materials_attributes: [:id, :name, :image, :description, :_destroy])
    end

    def manage_photo
        if params[:product][:photo]
            if params[:product][:photo][:three_sixty]
                @product.photos.where(three_sixty: true).destroy_all unless @product.new_record?
                params[:product][:photo][:three_sixty].each_with_index { |p, i| @product.photos.build(image: p, three_sixty: true, cover: (i == 0)) }
            end
            if params[:product][:photo][:extra]
                @product.photos.where(three_sixty: false).destroy_all unless @product.new_record?
                params[:product][:photo][:extra].each { |p| @product.photos.build(image: p) }
            end
        end
    end
end
