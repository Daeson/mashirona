class ApplicationController < ActionController::Base
    # Prevent CSRF attacks by raising an exception.
    # For APIs, you may want to use :null_session instead.
    protect_from_forgery with: :exception

    include CurrentVisitor

    private
    def after_sign_in_path_for(resource)
        console_path
    end

    def masked_authenticate!
        puts "+"*80
        puts "masked authentication excecuted! ;)"
        puts "+"*80
        raise ActionController::RoutingError.new('Not Found') unless admin_signed_in?
    end
end
