class AdminsController < ApplicationController
    before_action :set_admin, only: [:edit, :update, :destroy]
    before_action :masked_authenticate!

    def new
        @admin = Admin.new
    end

    def edit
    end

    def create
        @admin = Admin.new(admin_params)

        if @admin.save
            redirect_to console_url, notice: 'Admin was successfully created.'
        else
            render :new
        end
    end

    def update
        if admin_params[:password].blank?
            admin_params.delete(:password)
            admin_params.delete(:password_confirmation)
        end

        if admin_updated
            sign_in(:admin, @admin, bypass: true) if password_change?
            redirect_to console_url, notice: "Admin #{params[:id]} was successfully updated."
        else
            render :edit
        end
    end

    def destroy
        @admin.destroy
        redirect_to console_url, notice: 'Admin was successfully destroyed.'
    end

    private
    def set_admin
        @admin = Admin.find(params[:id])
    end

    def admin_params
        params.require(:admin).permit(:email, :password, :password_confirmation)
    end

    def password_change?
        admin_params[:password].present?
    end

    def admin_updated
        if password_change?
            return @admin.update_with_password(admin_params)
        else
            return @admin.update_without_password(admin_params)
        end
    end
end
