class CartItemsController < ApplicationController
  include CurrentCart
  before_action :get_cart!
  before_action :set_cart_item, only: [:update, :destroy]
  before_action :set_anchor

  def create
    respond_to do |format|
      @product = @cart.add_product(params[:product_id]).product
      format.html { redirect_to :back }
      format.js {  }
    end
  end

  def update
    respond_to do |format|
      @cart_item.update(cart_item_params)
      format.html { redirect_to :back }
      format.js {  }
    end
  end

  def destroy
    respond_to do |format|
      @product = @cart_item.destroy.product
      format.html { redirect_to :back }
      format.js {  }
    end
  end

  private
  def set_cart_item
    @cart_item = CartItem.find(params[:id])
  end

  def cart_item_params
    params.require(:cart_item).permit(:quantity)
  end

  def set_anchor
    @remove_item = params[:remove_item]
    @description = params[:description]
  end
end
