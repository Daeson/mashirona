class ConsoleController < ApplicationController
    before_action :masked_authenticate!

    def core
        @admins = Admin.all
        @products = Product.all
    end
end
